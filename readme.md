textgen
===========

textgen generator for Drupal.

Instructions
------------

textgen
Unpack in the *modules* folder (currently in the root of your Drupal
installation)

enable in `/admin/modules`.

visit `/admin/config/development/textgen` and enter your own set of
phrases from NovelAI to build random-generated text (or go with the default textgen).

Last, visit `www.example.com/textgen/generate/P/S` where:
- *P* is the number of *paragraphs*
- *S* is the maximum number of *sentences*

There is also a generator block in which you can choose how many paragraphs and
phrases and it'll do the rest.

If you need, there's also a specific *generate textgen* permission.

Attention
---------

work in progress
