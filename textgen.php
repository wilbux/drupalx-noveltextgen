<?php

use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Implements hook_help().
 */
function textgen_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    case 'help.page.textgen':
      return t('
        <h2>Text generator for Drupal.</h2>
        <h3>Instructions</h3>
        <p>Lorem ipsum dolor sit amet... attempt with NovelAI</p>
        <p>Unpack in the <em>modules</em> folder (currently in the root of your Drupal installation). Enable in <strong>/admin/modules</strong>.</p>
        <p>Visit <strong>/admin/config/development/textgen</strong> and enter your set of phrases from NovelAI to build random-generated text.</p>
        <p>Visit <strong>www.example.com/textgen/generate/P/S</strong> where:</p>
        <ul>
          <li><em>P</em> is the number of <em>paragraphs</em></li>
          <li><em>S</em> is the maximum number of <em>sentences</em></li>
        </ul>
        <p>There is also a generator block in which you can choose how many paragraphs and phrases and it will do the rest.</p>
        <p>If you need, there is also a specific <em>generate textgen</em> permission.</p>
        <h3>Attention</h3>
        <p> work in progress.</p>
      ');
  }
}
